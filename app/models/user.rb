class User < ApplicationRecord
  has_many :microposts
  #Make sure name is not blank
  validates :name, presence: true
  #Make sure email is not blank
  validates :email, presence: true
end
