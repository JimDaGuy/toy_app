class Micropost < ApplicationRecord
  belongs_to :user
  #make sure content is no longer than 140 char
  validates :content, length: { maximum: 140 },
  #make sure content contains content
                      presence: true
end
